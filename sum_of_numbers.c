//Write a program to find the sum of n different numbers using 4 functions
#include<stdio.h>

float input();
float sum(float a, float b);
float output(float c);

int main()
{
    int n ,i ;
    printf("how many numbers need to be added?");
    scanf("%d",&n);
    float a[n];
    for(i=0;i<n;i++)
    {
        printf("enter %d number : ",i+1);
        a[i]=input();
    }
    float b = 0;
    for(i=0;i<n;i++)
    {
        b = sum(b,a[i]);
    }
    output(b);
    return 0;
}
float input()
{
    float v;
    scanf("%f",&v);
    return v;
}

float sum(float a,float b)
{
    return(a+b);
}
  float output(float c)
  {
      printf("sum of given number : %f\n",c);
      return 0;
  }
