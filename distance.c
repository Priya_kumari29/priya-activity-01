//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>

float pointA(float x2, float x1)
{
        return x2-x1;
}

float pointB(float y2, float y1)
{
        return y2-y1;
}

float distance(float x1, float y1, float x2, float y2)
{
        return sqrt(pointA(x2, x1) * pointA(x2, x1) + pointB(y2, y1) * pointB(y2, y1));
}

float output(float x2, float x1, float y2, float y1)
{
        printf("%.2f",distance(x2,x1,y2,y1));
}

int main()
{
       float x2,x1,y2,y1;
       printf("Enter (x,y) coordinates of point A: ");
       scanf("%f%f",&x1,&y1);
       printf("Enter (x,y) coordinates of point B: ");
       scanf("%f%f",&x2,&y2);
       printf("The distance between point A(%.2f, %.2f) and point B(%.2f, %.2f) is:\n",x1,y1,x2,y2);
       output(x2,x1,y2,y1);
       return 0;
}